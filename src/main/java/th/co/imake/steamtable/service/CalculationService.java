package th.co.imake.steamtable.service;

import th.co.imake.steamtable.model.FormulaM;
import th.co.imake.steamtable.model.PatternM;

import java.util.List;

public interface CalculationService {
    public List<FormulaM> calculate(List<FormulaM> formulas);
    public String eval(String formula);

    public PatternM listPattern(PatternM pattern);
}
